# Webcounter
Simple Python Webcounter with redis server
## Build
docker build -t andrepreto/webcounter:1.0.1 .
## Dependencies
docker run -d  --name redis --rm redis:alpine
## Run
docker run -d --rm -p80:5000 --name webcounter --link redis -e REDIS_URL=redis andrepreto/webcounter:1.0.1

### Gitlab register
gitlab-runner register -n \
--url https://gitlab.com/ \
--executor shell \
--description "develop" \
--tag-list "develop" \
--registration-token GR1348941zyEYo6nZzVSmFn7RUUXs

### Run runner
gitlab-runner run

### Docker gitlab-runner
docker volume create gitlab-runner-config && 
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest

### Gitlab register in docker   
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register